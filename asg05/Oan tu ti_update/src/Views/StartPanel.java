/*
 * Created by JFormDesigner on Wed May 14 18:06:56 ICT 2014
 */

package Views;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Source.Computer;
import Source.Novice;
import Source.Vetaran;

/**
 * @author Bui Trung
 */
public class StartPanel extends JPanel {
	public StartPanel() {
		initComponents();
	}

	private void ModeActionPerformed(ActionEvent e) {
		// TODO add your code here
		if(e.getSource() == button1)
			computerMode = new Novice();
		if(e.getSource() == button2)
			computerMode = new Vetaran();
		playPanel.setComputerMode(computerMode);
		
		JFrame parent = Utitilities.findJFrameOf(this);
		
		if(parent != null)
		{
			parent.setContentPane(playPanel.getPanel());
			parent.pack();
		}
		else
		{
			JOptionPane.showMessageDialog(parent,"Panel ChoicePanel only used form JFrame");
			System.exit(1);
		}
		
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Bui Trung
		button1 = new JButton();
		button2 = new JButton();
		label1 = new JLabel();
		label2 = new JLabel();

		//======== this ========
		setBackground(new Color(204, 204, 204));
		/*
		// JFormDesigner evaluation mark
		setBorder(new javax.swing.border.CompoundBorder(
			new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
				"JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
				javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
				java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

		*/
		//---- button1 ----
		button1.setText("Novice");
		button1.setToolTipText("random model");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ModeActionPerformed(e);
			}
		});

		//---- button2 ----
		button2.setText("Vataran");
		button2.setToolTipText("intelligent computer");
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ModeActionPerformed(e);
			}
		});

		//---- label1 ----
		label1.setText("Start Game");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setFont(new Font("Tahoma", Font.BOLD, 36));
		label1.setForeground(Color.blue);
		label1.setBackground(new Color(204, 204, 204));

		//---- label2 ----
		label2.setText("Choose your component");
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label2.setForeground(Color.red);

		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup()
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addContainerGap(112, Short.MAX_VALUE)
					.addGroup(layout.createParallelGroup()
						.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
							.addComponent(label1, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
							.addGap(111, 111, 111))
						.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
							.addComponent(label2, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
							.addGap(160, 160, 160))))
				.addGroup(layout.createSequentialGroup()
					.addGap(180, 180, 180)
					.addGroup(layout.createParallelGroup()
						.addComponent(button2, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
						.addComponent(button1, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
					.addGap(0, 186, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addContainerGap(72, Short.MAX_VALUE)
					.addComponent(label1, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
					.addGap(34, 34, 34)
					.addComponent(label2)
					.addGap(28, 28, 28)
					.addComponent(button1, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(27, 27, 27)
					.addComponent(button2, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addGap(74, 74, 74))
		);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}
	
	public Computer getComputerMode(){
		return computerMode;
	}
	public void setPlayPanel(ChoicePanel playPanel){
		this.playPanel = playPanel;
	}
	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Bui Trung
	private JButton button1;
	private JButton button2;
	private JLabel label1;
	private JLabel label2;
	
	private ChoicePanel playPanel; 
	private Computer computerMode;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
