import java.util.List;
import java.util.Random;


public class Vetaran extends Computer {
	public static int count = 0;
	
	public Choice makeChoice(){
		Choice computerChoice = null;

		if(count ==0 || count ==1 || count ==2)
		{
			Random random = new Random();
			int choice = random.nextInt(3);
			computerChoice = Choice.values()[choice];
			
			count++;
		}
		else if(count == 3|| count == 4)
		{
			List<String> list = SavingData.getPlayerChoice();
			String lastPlayerChoice = list.get(list.size()-1);
			
			if(lastPlayerChoice.equals("rock"))
				computerChoice = Choice.Paper;
			
			if(lastPlayerChoice.equals("paper"))
				computerChoice = Choice.Scissor;
			
			if(lastPlayerChoice.equals("scissor"))
				computerChoice = Choice.Rock;
			count++;
		}
		else
		{
			List<String> list = SavingData.getPlayerChoice();
			
			int countRock=0;
			int countPaper=0;
			int countScissor=0;
			
			for(int i=0;i<list.size();i++)
			{
				if(list.get(i).equals("rock"))
					countRock++;
				if(list.get(i).equals("paper"))
					countPaper++;
				if(list.get(i).equals("sicssor"))
					countScissor++;
			}
			
			int max = MaxOf(countRock,countPaper,countScissor);
			
			if(countRock == max)
				computerChoice = Choice.Paper;
			else if(countPaper == max)
				computerChoice = Choice.Scissor;
			else if(countScissor == max)
				computerChoice = Choice.Rock;
			
			count++;
		}
		
		return computerChoice;
		
	}//end method makeChoice
	
	private int MaxOf(int x,int y,int z)
	{
		int max =x;
		if(y>max)
			max =y;
		if(z>max)
			max=z;
		
		return max;
	}
}
