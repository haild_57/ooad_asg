import java.util.LinkedList;
import java.util.List;


public class SavingData {
	
	private static List<String> PlayerChoice;
	private static List<String> ComputerChoice;
	private static List<KetQua> ketqua;
	
	public static void initiateData(){
		PlayerChoice = new LinkedList<String>();
		ComputerChoice  = new LinkedList<String>();
		ketqua  = new LinkedList<KetQua>();
		
	}
	public static void Saving(Round round){
		PlayerChoice.add(round.getPlayerChoice().toString());
		ComputerChoice.add(round.getComputerChoice().toString());
		ketqua.add(round.Check());
	}
	public static List<String> getPlayerChoice(){
		return PlayerChoice;
	}
	public static List<String> getComputerChoice(){
		return ComputerChoice;
	}
	public static List<KetQua> getKetQua(){
		return ketqua;
	}
}
